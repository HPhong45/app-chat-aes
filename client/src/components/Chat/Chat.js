import React, { useState, useEffect } from 'react';
import queryString from 'query-string';
import io from 'socket.io-client';
import assert from 'assert';

import TextContainer from '../TextContainer/TextContainer';
import Messages from '../Messages/Messages';
import InfoBar from '../InfoBar/InfoBar';
import Input from '../Input/Input';

import './Chat.css';
const crypto = require('crypto');

const CryptoJS = require('crypto-js');

const ENDPOINT = 'localhost:5000';

const key = '123456';

let socket;

const Chat = ({ location }) => {
  const [name, setName] = useState('');
  const [room, setRoom] = useState('');
  const [users, setUsers] = useState('');
  const [message, setMessage] = useState('');
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    console.log(queryString.parse(location.search));
    const { name, room } = queryString.parse(location.search);

    socket = io(ENDPOINT);
    setRoom(room);
    setName(name);
    console.log(name);

    socket.emit('join', { name, room }, (error) => {
      if (error) {
        alert(error);
      }
    });
  }, [ENDPOINT, location.search]);

  useEffect(() => {
    socket.on('message', (obj) => {
      // Decrypt
      console.log(obj);
      if (obj.user !== 'admin') {
        const bytes = CryptoJS.AES.decrypt(obj.text, 'my-secret-key@123');
        console.log(bytes);
        console.log(name);
        obj.text = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
      }
      setMessages((messages) => [...messages, obj]);
    });

    socket.on('roomData', ({ users }) => {
      setUsers(users);
    });
  }, []);

  const sendMessage = (event) => {
    event.preventDefault();
    if (message) {
      // Encode here
      const ciphertext = CryptoJS.AES.encrypt(JSON.stringify(message), 'my-secret-key@123').toString();
      console.log(ciphertext);
      socket.emit('sendMessage', ciphertext, () => setMessage(''));
    }
  };

  const test = () => {
    const alice = crypto.getDiffieHellman('modp15');
    const bob = crypto.getDiffieHellman('modp15');
    alice.generateKeys();
    bob.generateKeys();

    console.log(bob.getPublicKey().toString('hex'));
    console.log(alice.getPublicKey().toString('hex'));

    // Exchange and generate the secret...
    const aliceSecret = Buffer.from(alice.computeSecret(bob.getPublicKey())).toString('hex');
    const bobSecret = Buffer.from(bob.computeSecret(alice.getPublicKey())).toString('hex');

    console.log(bobSecret === aliceSecret)
  };

  return (
    <div className="outerContainer">
      <div className="container">
        <InfoBar room={room} />
        <Messages messages={messages} name={name} />
        <div onClick={test}>heheheheheheehe</div>
        <Input message={message} setMessage={setMessage} sendMessage={sendMessage} />
      </div>
      <TextContainer users={users} />
    </div>
  );
};

export default Chat;
